# Bethesda Twitch/Tiltify Integration (BTTI)
Allows for Twitch/Tiltify integration with different Bethesda games for streaming purposes.

## Supported Games
- Fallout 4

## Further documentation 
We have more information and an installation guide at [http://artifacts.forgottendream.co.uk/](http://artifacts.forgottendream.co.uk/)

## Credits
This is based on the Twitch/Tiltify bot code found in [bethesdaTwitchIntegration](https://gitlab.com/Flenarn/bethesdatwitchintegration) by **Flenarn**.

Credit is also due to **Bethesda** for the games and for their support of the modding community.

