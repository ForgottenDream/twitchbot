# Links
 - [Original Project](https://gitlab.com/Flenarn/bethesdatwitchintegration)
 - [OG Project Docs](https://flenarn.gitlab.io/BTI/)
 - [Bootstrap](https://getbootstrap.com/docs/5.1/getting-started/introduction/)
 - [Papyrus Scripting](https://github.com/joelday/papyrus-lang/wiki/Papyrus)
 - [F4SE](https://github.com/ianpatt/f4se)
 - [Websocket Lib C++](https://machinezone.github.io/IXWebSocket/usage/#websocket-client-api)
 - [Websocket Lib Java](https://github.com/TooTallNate/Java-WebSocket)
