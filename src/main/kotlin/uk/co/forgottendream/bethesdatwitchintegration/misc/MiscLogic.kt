package uk.co.forgottendream.bethesdatwitchintegration.misc

import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logCatch
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastCommandGlobal
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastDefaultCommand
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastSpecialCommand
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import uk.co.forgottendream.bethesdatwitchintegration.networking.MessagingServer

import java.io.File
import java.io.FileNotFoundException
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.Paths

object MiscLogic {
    fun isInteger(stringToCheck: String?): Boolean {
        if (stringToCheck == null) return false
        val length: Int = stringToCheck.length
        if (length == 0) return false
        var i = 0

        if (stringToCheck[0] == '.') {
            if (length == 1) return false
            i = 1
        }

        while (i < length) {
            val c: Char = stringToCheck[i]
            if (c < '0' || c > '9') return false
            i++
        }

        return true
    }

    /*
     * Creates a command for script extender to detect and execute.
     */
    fun createCommandCall(command: String?) {
        if (command != null) {
            BethesdaTwitchIntegration.messagingServer.sendCommandMessage(command)
        }
    }
}
