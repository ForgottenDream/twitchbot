package uk.co.forgottendream.bethesdatwitchintegration.networking

import com.fasterxml.jackson.databind.ObjectMapper
import org.java_websocket.WebSocket
import org.java_websocket.drafts.Draft
import org.java_websocket.exceptions.InvalidDataException
import org.java_websocket.framing.CloseFrame
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.handshake.ServerHandshakeBuilder
import org.java_websocket.server.WebSocketServer
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logCatch
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logInfo
import uk.co.forgottendream.bethesdatwitchintegration.commands.SpawnCommand
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import uk.co.forgottendream.bethesdatwitchintegration.tiltify.DonationManagement
import java.net.InetSocketAddress

class MessagingServer(port: Int) : WebSocketServer(InetSocketAddress(port)) {
    private val mapper = ObjectMapper()
    val donations = mutableMapOf<String, Donation>()
    val twitchSpawns = mutableMapOf<String, TwitchCommand>()

    @Throws(InvalidDataException::class)
    override fun onWebsocketHandshakeReceivedAsServer(
        conn: WebSocket,
        draft: Draft,
        request: ClientHandshake
    ): ServerHandshakeBuilder {
        val builder = super.onWebsocketHandshakeReceivedAsServer(conn, draft, request)

        //If there are no cookies set reject it as well.
        if (!request.hasFieldValue("authorization")) {
            throw InvalidDataException(CloseFrame.POLICY_VALIDATION, "Not accepted!")
        }

        //If the cookie does not contain a specific value
        if (request.getFieldValue("authorization") != settings.getAuthorization()) {
            throw InvalidDataException(CloseFrame.POLICY_VALIDATION, "Not accepted!")
        }

        //If the cookie does not contain a specific value
        if (request.getFieldValue("authorization") == "btti-plugin") {
            logInfo(
                "Messaging",
                "You are using the default authorization string. You should change this to something only " +
                        "YOU know in the settings.json for this and the Fallout4_BTTI_Plugin.toml for the plugin."
            )
        }

        return builder
    }

    override fun onOpen(conn: WebSocket, handshake: ClientHandshake) {
        conn.setAttachment(PLUGIN_CONNECTION)
        logInfo("Messaging", "Plugin has connected.")
    }

    override fun onClose(conn: WebSocket, code: Int, reason: String, remote: Boolean) {
        logInfo("Messaging", "Plugin has disconnected.")
    }

    override fun onMessage(conn: WebSocket, message: String) {
        val msgJson = mapper.readTree(message)

        when (msgJson.fieldNames().next()) {
            PLAYER_INSIDE -> {
                val uuid = msgJson[UUID].asText()
                val isInside = msgJson[PLAYER_INSIDE].asBoolean()
                logInfo("Messaging", "Received playerInside packet: $message")

                if (twitchSpawns.containsKey(uuid)) {
                    val command = twitchSpawns[uuid]

                    if (command != null) {
                        SpawnCommand.handleSpawn(command.username, command.command, isInside)
                        twitchSpawns.remove(uuid)
                    }
                } else if (donations.containsKey(uuid)) {
                    val donation = donations[uuid]

                    if (donation != null) {
                        DonationManagement.handleDonation(
                            donation.donationAmount,
                            donation.tag,
                            donation.name,
                            isInside
                        )
                        donations.remove(uuid)
                    }
                }
            }
            else -> logInfo("Messaging", "Received unknown packet: $message")
        }
    }

    override fun onError(conn: WebSocket, ex: Exception) {
        logCatch("Messaging", ex)
    }

    override fun onStart() {
        logInfo("Messaging", "Server started")
        connectionLostTimeout = 0
        connectionLostTimeout = 100
    }

    fun sendIsInteriorMessage(uuid: String) {
        val message = mapper.createObjectNode()
        message.put(REQUEST, PLAYER_INSIDE)
        message.put(UUID, uuid)
        val connection = super.getConnections().find { conn -> conn.getAttachment<String>() == PLUGIN_CONNECTION }
        connection?.send(message.toString())
    }

    fun sendCommandMessage(command: String) {
        val message = mapper.createObjectNode()
        message.put(COMMAND, command)
        val connection = super.getConnections().find { conn -> conn.getAttachment<String>() == PLUGIN_CONNECTION }
        connection?.send(message.toString())
    }

    companion object {
        const val PLUGIN_CONNECTION = "PLUGIN_CONNECTION"
        const val REQUEST = "request"
        const val COMMAND = "command"
        const val PLAYER_INSIDE = "isPlayerInside"
        const val UUID = "uuid"
    }
}
