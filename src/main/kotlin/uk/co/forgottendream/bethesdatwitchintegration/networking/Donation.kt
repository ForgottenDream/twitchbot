package uk.co.forgottendream.bethesdatwitchintegration.networking

data class Donation(val donationAmount: Float, val tag: String, val name: String)
