package uk.co.forgottendream.bethesdatwitchintegration.networking

data class TwitchCommand(val username: String, val command: String)
