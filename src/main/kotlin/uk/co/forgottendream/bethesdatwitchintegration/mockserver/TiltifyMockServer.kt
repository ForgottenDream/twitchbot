package uk.co.forgottendream.bethesdatwitchintegration.mockserver

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import org.mockserver.configuration.Configuration
import org.mockserver.integration.ClientAndServer
import org.mockserver.integration.ClientAndServer.startClientAndServer
import org.mockserver.logging.MockServerLogger
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.mockserver.socket.tls.KeyStoreFactory
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.getFileFromResourceAsStream
import java.lang.System.currentTimeMillis
import java.nio.charset.StandardCharsets
import javax.net.ssl.HttpsURLConnection

class TiltifyMockServer {
    init {
        val keyStoreFactory = KeyStoreFactory(Configuration.configuration(), MockServerLogger())
        HttpsURLConnection.setDefaultSSLSocketFactory(keyStoreFactory.sslContext().socketFactory)
        mockServer = startClientAndServer(1080)
        val mockResponse = getFileFromResourceAsStream("tiltifyInitial.json")
        val text = String(mockResponse.readAllBytes(), StandardCharsets.UTF_8)
        mockServer!!.`when`(request().withPath("/api/v3/campaigns/.*/donations").withQueryStringParameter("count", "1"))
            .respond(
                response()
                    .withBody(text)
            )

        defineRunningExpectation(100)
    }

    companion object {
        var mockServer: ClientAndServer? = null
        private val runningResponseFile = getFileFromResourceAsStream("tiltifyRunning.json")
        private val donationDataFile = getFileFromResourceAsStream("tilitifyDonation.json")
        private val runningResponse = String(runningResponseFile.readAllBytes(), StandardCharsets.UTF_8)
        private val donationData = String(donationDataFile.readAllBytes(), StandardCharsets.UTF_8)
        private val mapper = ObjectMapper()
        private val json = mapper.readTree(runningResponse)
        private var currentId = 100

        fun getResponseData(): String {
            return json.toPrettyString()
        }

        fun blankResponseData() {
            val dataArray = json["data"] as ArrayNode
            dataArray.remove(0)
        }

        fun createData(amount: Float, name: String, comment: String = "") {
            val dataArray = json["data"] as ArrayNode
            val newNode = mapper.readTree(donationData) as ObjectNode
            newNode.put("id", currentId + 1)
            newNode.put("amount", amount)
            newNode.put("name", name)
            newNode.put("comment", comment.ifEmpty { null })
            newNode.put("completedAt", currentTimeMillis())
            newNode.put("updatedAt", currentTimeMillis())
            dataArray.remove(0)
            dataArray.add(newNode)
            defineRunningExpectation(currentId)
            currentId++
        }

        fun defineRunningExpectation(after: Int) {
            val fetchExpectation = mockServer!!.`when`(
                request().withPath("/api/v3/campaigns/.*/donations").withQueryStringParameter("after", "$after")
            )
            fetchExpectation.withId("fetch").respond(
                response()
                    .withBody(getResponseData())
            )
        }
    }
}
