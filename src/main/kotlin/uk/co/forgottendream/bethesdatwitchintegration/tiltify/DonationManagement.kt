package uk.co.forgottendream.bethesdatwitchintegration.tiltify

import com.fasterxml.jackson.databind.ObjectMapper
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logCatch
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logInfo
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import uk.co.forgottendream.bethesdatwitchintegration.misc.MiscLogic
import java.io.FileReader
import java.util.*

object DonationManagement {
    fun handleDonation(donationAmount: Float, tag: String, name: String, isInside: Boolean) {
        val donationCommand = getDonationListingEntry(donationAmount, tag, name, isInside)
        if (donationCommand != "null") {
            MiscLogic.createCommandCall(donationCommand)
            Thread.sleep(650)
        }
    }

    private fun getDonationListingEntry(donationAmount: Float, tag: String, name: String, isInside: Boolean): String {
        val objectMapper = ObjectMapper()

        try {
            val filePath = "data/${settings.gameJson}/${settings.gameJson}tiltify.json"
            val jsonNode = objectMapper.readTree(FileReader(filePath))
            val breakpointSet = mutableSetOf<Float>()

            jsonNode.fieldNames().forEach { key ->
                breakpointSet.add(key.toFloat())
            }

            breakpointSet.sorted().forEach { donationBreak ->
                if (donationAmount <= donationBreak || donationAmount > breakpointSet.last()) {
                    val donationNode = jsonNode[String.format("%.2f", donationBreak)]
                    var commands = donationNode[tag].toList()

                    if (isInside) {
                        commands = commands.filter { entry ->
                            val command = entry[entry.fieldNames().next()]
                            var retVal = true
                            if (command[0].asText() == "NPC") {
                                retVal = command[3].asBoolean()
                            }
                            retVal
                        }
                    }

                    val randomCommandIndex = Random().nextInt(commands.size)
                    val commandEntry = commands[randomCommandIndex]
                    val commandName = commandEntry.fieldNames().next()
                    val commandNode = commandEntry[commandName]

                    val type = commandNode[0].asText()

                    when {
                        type.equals("ITEM", ignoreCase = true) -> {
                            val id = commandNode[1].asText()
                            val amount = commandNode[2].asInt()
                            val pluginToLookFor = if (commandNode.size() == 4) {
                                (commandNode[3] as String?)!!
                            } else settings.defaultPlugin

                            return "player.additem $id $amount $pluginToLookFor"
                        }
                        type.equals("NPC", ignoreCase = true) -> {
                            val id = commandNode[1].asText()
                            val amount = commandNode[2].asInt()
                            val allowedInInterior = if (commandNode[3].asBoolean()) 1 else 0
                            val pluginToLookFor = if (commandNode.size() == 5) {
                                commandNode[4].asText()
                            } else settings.defaultPlugin

                            val msg = "$name trying to spawn $amount $commandName"
                            logInfo("Tiltify (donation spawn)", msg)
                            return "spawn $id $amount $allowedInInterior $pluginToLookFor"
                        }
                        type.equals("COMMAND", ignoreCase = true) -> {
                            return commandNode[1].asText()
                        }
                    }
                }
            }
        } catch (exception: Exception) {
            logCatch("Donation Manager", exception)
        }

        return "null"
    }
}
