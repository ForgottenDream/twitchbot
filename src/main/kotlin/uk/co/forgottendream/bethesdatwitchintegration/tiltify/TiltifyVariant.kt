package uk.co.forgottendream.bethesdatwitchintegration.tiltify

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.HttpHeaders
import org.apache.http.HttpHost
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.DefaultProxyRoutePlanner
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logCatch
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logError
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logInfo
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import uk.co.forgottendream.bethesdatwitchintegration.mockserver.TiltifyMockServer
import uk.co.forgottendream.bethesdatwitchintegration.mockserver.TiltifyMockServer.Companion.blankResponseData
import uk.co.forgottendream.bethesdatwitchintegration.networking.Donation
import java.io.BufferedReader
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*

object TiltifyVariant {
    //How often we want the timer to run, by default every 2 seconds.
    private val tiltifySettings = settings.getTiltifySettings()
    var lastIdChecked = 0
    var initialBoot = true
    private val token = "Bearer ${tiltifySettings.applicationToken}"
    private val httpClient: HttpClient
    private val mapper = ObjectMapper()

    init {
        val config = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build()
        httpClient = if (tiltifySettings.mockApi) {
            val httpHost = HttpHost("localhost", 1080)
            val defaultProxyRoutePlanner = DefaultProxyRoutePlanner(httpHost)
            HttpClients.custom().setRoutePlanner(defaultProxyRoutePlanner).setDefaultRequestConfig(config).build()
        } else {
            HttpClients.custom().setDefaultRequestConfig(config).build()
        }
    }

    fun tiltifyLoop() {
        val mainTimer = Timer()

        mainTimer.schedule(object : TimerTask() {
            override fun run() {
                if (initialBoot) {
                    initialBoot = false
                    val campaignId = tiltifySettings.campaignId
                    val uriString = "https://tiltify.com/api/v3/campaigns/$campaignId/donations?count=1"
                    val responseJson = doGetRequest(uriString)
                    if (responseJson != null) {
                        val dataArray = responseJson.withArray<JsonNode>("data")
                        val donation = dataArray[0]

                        if (donation != null) lastIdChecked = donation["id"].asInt() else 0
                    }

                    logInfo("Tiltify", "Initialization done!")
                    logInfo("Tiltify", "Starting from donation ID: $lastIdChecked")
                }

                // We prep the request string here, based on if it's the first time we're doing the donation check or not.
                val campaignId = tiltifySettings.campaignId
                val requestString = "https://tiltify.com/api/v3/campaigns/$campaignId/donations?&after=$lastIdChecked"

                val responseJson = doGetRequest(requestString)

                if (responseJson != null) {
                    val dataArray = responseJson.withArray<JsonNode>("data")

                    if (!dataArray.isEmpty) {
                        logInfo(message = "=======================================================")
                        for (donationNode in dataArray) {
                            val id = donationNode["id"].asText()
                            val amount = donationNode["amount"].asText()
                            val name = donationNode["name"].asText()
                            val time = getTimeStamp(donationNode["completedAt"].asLong())
                            val comment = donationNode["comment"].asText()

                            logInfo("Tiltify", "ID in array order: $id, amount: $$amount")
                            lastIdChecked = donationNode["id"].asInt()

                            logInfo(message = "-----------------------------------------------------------------")
                            logInfo("Tiltify", "Donation of $$amount from $name at $time: $comment")
                            logInfo(message = "-----------------------------------------------------------------")

                            if (tiltifySettings.mockApi) {
                                blankResponseData()
                                TiltifyMockServer.defineRunningExpectation(lastIdChecked)
                            }

                            val donationAmount = donationNode["amount"].asDouble().toFloat()
                            var tag = "${tiltifySettings.tagPrefix}${tiltifySettings.defaultTag}"

                            if (donationNode["comment"] != null) {
                                val commentChunks = donationNode["comment"].asText().split(" ")
                                tag = commentChunks.firstOrNull() { chunk ->
                                    chunk.startsWith(tiltifySettings.tagPrefix)
                                }?.substringAfter(tiltifySettings.tagPrefix) ?: tiltifySettings.defaultTag

                            }

                            val uuid = UUID.randomUUID().toString()
                            BethesdaTwitchIntegration.messagingServer.donations[uuid] =
                                Donation(donationAmount, tag, name)
                            BethesdaTwitchIntegration.messagingServer.sendIsInteriorMessage(uuid)
                        }
                    }
                } else {
                    logError("Tiltify", "CRITICAL FAILURE - TILTIFY API 403/404 RESPONSE")
                }

            }
        }, 0, (tiltifySettings.pollingFrequency * 1000).toLong())
    }

    private fun doGetRequest(uriString: String): JsonNode? {
        try {
            // Do the call
            val uriBuilder = URIBuilder(uriString)
            val httpGet = HttpGet(uriBuilder.build())
            httpGet.setHeader(HttpHeaders.AUTHORIZATION, token)
            httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json")
            val httpResponse: HttpResponse = httpClient.execute(httpGet)
            val httpEntity = httpResponse.entity

            if (httpEntity != null) {
                // Parse the response to a string
                val reader = BufferedReader(InputStreamReader(httpEntity.content))
                var output: String?
                val response = StringBuilder()

                while (reader.readLine().also { output = it } != null) {
                    response.append(output)
                }

                reader.close()

                // Return json node
                return mapper.readTree(response.toString())
            }
        } catch (e: Exception) {
            logCatch("Tiltify (doGetRequest)", e)
        }

        return null
    }

    private fun getTimeStamp(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("dd/MM/yyyy HH:mm")
        return format.format(date)
    }
}
