package uk.co.forgottendream.bethesdatwitchintegration.json

import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings

object TimedLogic {
    fun defaultPointLoop() {
        for (userToModify in PointLogic.getActiveUsers()) {
            val pointsPerMinute = settings.getTwitchSettings().getPointsSettings().perMinute
            PointLogic.modifyUserPoints(pointsToModify = pointsPerMinute, username = userToModify)
        }
    }

    fun addUser(userName: String) {
            if (!PointLogic.userExists(userName)) {
                PointLogic.addUser(userName)
            }
    }
}
