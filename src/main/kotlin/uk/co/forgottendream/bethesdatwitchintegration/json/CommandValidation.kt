package uk.co.forgottendream.bethesdatwitchintegration.json

import com.fasterxml.jackson.databind.ObjectMapper
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logCatch
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import java.io.FileReader

object CommandValidation {
    // Item Command Properties
    var itemCost = 0
    var itemID = "null"
    var itemsMultiplied = false

    // Spawn Command Properties
    var npcCost = 0
    var npcToSpawn = "null"
    var npcAmountToSpawn = 0
    var allowedInInterior = true

    // Weather Command Properties
    var weatherID = "null"
    var weatherCost = 0

    // Misc Command Properties
    var miscCost = 0
    var miscCommand = "null"

    // Misc Properties
    private var defaultPlugin = settings.defaultPlugin
    var pluginToLookFor = "null"

    fun validateSpawn(spawnCommand: String?): Boolean {
        val objectMapper = ObjectMapper()

        try {
            val filePath = "data/${settings.gameJson}/${settings.gameJson}spawns.json"
            val jsonNode = objectMapper.readTree(FileReader(filePath))

            for (fieldName in jsonNode.fieldNames()) {
                if (fieldName.equals(spawnCommand, ignoreCase = true)) {
                    val jsonArray = jsonNode[fieldName]
                    npcCost = jsonArray[0].asInt()
                    npcToSpawn = jsonArray[1].asText()
                    npcAmountToSpawn = jsonArray[2].asInt()
                    allowedInInterior = jsonArray[3].asBoolean()
                    pluginToLookFor = if (jsonArray.size() == 5) jsonArray[4].asText() else defaultPlugin
                    return true
                }
            }

            return false
        } catch (exception: Exception) {
            logCatch("Command Validation (spawn)", exception)
        }

        return false
    }

    fun validateMisc(miscCommand: String?): Boolean {
        val objectMapper = ObjectMapper()

        try {
            val filePath = "data/${settings.gameJson}/${settings.gameJson}misc.json"
            val jsonNode = objectMapper.readTree(FileReader(filePath))

            for (fieldName in jsonNode.fieldNames()) {
                if (fieldName.equals(miscCommand, ignoreCase = true)) {
                    val jsonArray = jsonNode[fieldName]
                    miscCost = jsonArray[0].asInt()
                    CommandValidation.miscCommand = jsonArray[1].asText()
                    return true
                }
            }

            return false
        } catch (exception: Exception) {
            logCatch("Command Validation (misc)", exception)
        }

        return false
    }

    fun validateWeather(weatherCommand: String?): Boolean {
        val objectMapper = ObjectMapper()

        try {
            val filePath = "data/${settings.gameJson}/${settings.gameJson}weathers.json"
            val jsonNode = objectMapper.readTree(FileReader(filePath))

            for (fieldName in jsonNode.fieldNames()) {
                if (fieldName.equals(weatherCommand, ignoreCase = true)) {
                    val jsonArray = jsonNode[fieldName]
                    weatherCost = jsonArray[0].asInt()
                    weatherID = jsonArray[1].asText()
                    pluginToLookFor = if (jsonArray.size() == 3) jsonArray[2].asText() else defaultPlugin
                    return true
                }
            }

            return false
        } catch (exception: Exception) {
            logCatch("Command Validation (weather)", exception)
        }

        return false
    }

    fun validateGive(command: String): Boolean {
        val objectMapper = ObjectMapper()

        try {
            val filePath = "data/${settings.gameJson}/${settings.gameJson}items.json"
            val jsonNode = objectMapper.readTree(FileReader(filePath))

            for (fieldName in jsonNode.fieldNames()) {
                if (fieldName.equals(command, ignoreCase = true)) {
                    val jsonArray = jsonNode[fieldName]
                    itemCost = jsonArray[0].asInt()
                    itemsMultiplied = command.startsWith("ammo_") || command.startsWith("resource_")
                    itemID = jsonArray[1].asText()
                    pluginToLookFor = if (jsonArray.size() == 3) jsonArray[2].asText() else defaultPlugin
                    return true
                }
            }

            return false
        } catch (exception: Exception) {
            logCatch("Command Validation (give)", exception)
        }
        return false
    }
}
