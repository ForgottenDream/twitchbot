package uk.co.forgottendream.bethesdatwitchintegration.json

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.github.twitch4j.TwitchClient
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logCatch
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.util.*

object PointLogic {
    private const val filePath = "data/json_viewers.json"

    private fun parseUsers(): JsonNode {
        if (!File(filePath).exists()) {
            val myWriter = FileWriter(filePath)
            myWriter.write("{}")
            myWriter.close()
        }

        val mapper = ObjectMapper()
        return mapper.readTree(FileReader(filePath))
    }

    private fun writeViewers(jsonNode: JsonNode) {
        try {
            val fileWriter = FileWriter(filePath)
            fileWriter.write(jsonNode.toPrettyString())
            fileWriter.close()
        } catch (exception: Exception) {
            logCatch("PointLogic (writeViewers)", exception)
        }
    }

    fun getActiveUsers(): List<String> {
        val keys = mutableListOf<String>()
        val iterator: Iterator<String> = parseUsers().fieldNames()
        iterator.forEachRemaining { userName ->
            if (getActiveState(userName))
            keys.add(userName)
        }
        return keys
    }

    fun purgeUsers(value: Int) {
        val jsonNode = parseUsers()

        for (userName in jsonNode.fieldNames()) {
            val objectNode = jsonNode as ObjectNode
            val currentEntryPoints = objectNode.get(userName).get("points").asInt()
            if (currentEntryPoints < value) {
                removeUser(userName)
            }
        }
    }

    fun userExists(username: String): Boolean {
        val sanitizedName = username.lowercase(Locale.getDefault())
        return parseUsers().findValue(sanitizedName) != null
    }

    fun initActiveState(broadcasterId: String, twitchClient: TwitchClient) {
        val authToken = Settings.settings.getTwitchSettings().botAuthToken
        var page: String? = null
        var chatters = twitchClient.helix.getChatters(
            authToken, broadcasterId,
            BethesdaTwitchIntegration.botUserId, 1000, page
        ).execute()

        parseUsers().fieldNames().forEachRemaining { userName ->
            setActiveState(userName, false)
        }

        for (chatter in chatters.chatters) {
            if (userExists(chatter.userName)) {
                setActiveState(chatter.userName, true)
            }
        }

        while (chatters.pagination.cursor != null) {
            page = chatters.pagination.cursor
            chatters = twitchClient.helix.getChatters(
                authToken, broadcasterId,
                BethesdaTwitchIntegration.botUserId, 1000, page
            ).execute()

            for (chatter in chatters.chatters) {
                if (userExists(chatter.userName)) {
                    setActiveState(chatter.userName, true)
                }
            }
        }
    }

    fun addUser(username: String) {
        val sanitizedName = username.lowercase(Locale.getDefault())
        val jsonNode = parseUsers()

        if (!userExists(sanitizedName)) {
            val objectNode = jsonNode as ObjectNode
            val userInfo: JsonNode = ObjectMapper().readTree("{\"isActive\": true, \"points\": 0 }")

            objectNode.put(sanitizedName, userInfo)
            writeViewers(objectNode)
        }
    }

    private fun removeUser(username: String) {
        val sanitizedName = username.lowercase(Locale.getDefault())
        val jsonNode = parseUsers()

        for (userNode in jsonNode.fieldNames()) {
            if (userNode.equals(sanitizedName, ignoreCase = true)) {
                val objectNode = jsonNode as ObjectNode
                objectNode.remove(sanitizedName)
                writeViewers(objectNode)
            }
        }
    }

    fun modifyUserPoints(pointsToModify: Int, username: String) {
        val sanitizedName = username.lowercase(Locale.getDefault())
        if (userExists(sanitizedName)) {
            val jsonNode = parseUsers()
            val objectNode = jsonNode as ObjectNode
            val userData = objectNode.get(sanitizedName) as ObjectNode
            val pointsNode = userData.get("points")
            val points = pointsNode.asInt() + pointsToModify
            userData.put("points", points)
            objectNode.put(sanitizedName, userData)
            writeViewers(objectNode)
        }
    }

    fun userPoints(username: String): Int {
        val sanitizedName = username.lowercase(Locale.getDefault())
        if (userExists(sanitizedName)) {
            val jsonNode = parseUsers()
            val objectNode = jsonNode as ObjectNode
            return objectNode.get(sanitizedName).get("points").asInt()
        }

        return 0
    }

    fun setActiveState(username: String, state: Boolean) {
        val sanitizedName = username.lowercase(Locale.getDefault())
        if (userExists(sanitizedName)) {
            val jsonNode = parseUsers()
            val objectNode = jsonNode as ObjectNode
            val userData = objectNode.get(sanitizedName) as ObjectNode
            userData.put("isActive", state)
            objectNode.put(sanitizedName, userData)
            writeViewers(objectNode)
        }
    }

    fun getActiveState(username: String): Boolean {
        val sanitizedName = username.lowercase(Locale.getDefault())
        if (userExists(sanitizedName)) {
            val jsonNode = parseUsers()
            val objectNode = jsonNode as ObjectNode
            return objectNode.get(sanitizedName).get("isActive").asBoolean()
        }

        return false
    }
}
