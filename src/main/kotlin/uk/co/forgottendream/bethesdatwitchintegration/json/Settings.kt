package uk.co.forgottendream.bethesdatwitchintegration.json

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration.Companion.logCatch
import java.io.FileReader
import java.io.IOException
import java.util.*

class Settings {
    private val mapper = ObjectMapper()
    private lateinit var jsonNode: JsonNode
    var systemEnabled = false
    var gameJson = "null"
    var defaultPlugin = "null"
    var gamePath = "null"

    init {
        refreshSettings()
    }

    fun getPort(): Int {
        return jsonNode["port"].asInt(8080)
    }

    fun getAuthorization(): String {
        return jsonNode["authorization"].asText("btti-plugin")
    }

    fun getTwitchSettings(): TwitchSettings {
        return TwitchSettings(jsonNode["twitch"])
    }

    fun getTiltifySettings(): TiltifySettings {
        return TiltifySettings(jsonNode["tiltify"])
    }

    fun refreshSettings(): Boolean {
        val settingsFile = FileReader("data/settings.json")

        try {
            jsonNode = mapper.readTree(settingsFile)
            setGameVariables()
            return true
        } catch (e: IOException) {
            logCatch("Settings (refreshSettings)", e)
        }

        return false
    }

    private fun setGameVariables() {
        when (jsonNode["gamemode"].asText("fallout4")) {
            "fallout4" -> {
                gameJson = "fallout4"
                defaultPlugin = "Fallout4.esm"
                gamePath = "Fallout4/F4SE"
            }

            "skyrimle" -> {
                gameJson = "skyrimLE"
                defaultPlugin = "Skyrim.esm"
                gamePath = "Skyrim/SKSE"
            }

            "skyrimse" -> {
                gameJson = "skyrimSE"
                defaultPlugin = "Skyrim.esm"
                gamePath = "Skyrim Special Edition/SKSE"
            }

            "falloutnewvegas" -> {
                gameJson = "newVegas"
                defaultPlugin = "FalloutNV.esm"
                gamePath = "FalloutNV/NVSE"
            }

            "fallout3" -> {
                gameJson = "fallout3"
                defaultPlugin = "Fallout3.esm"
                gamePath = "Fallout3/FOSE"
            }

            "oblivion" -> {
                gameJson = "oblivion"
                defaultPlugin = "Oblivion.esm"
                gamePath = "Oblivion/OBSE"
            }
        }
    }

    class TwitchSettings(private val node: JsonNode) {
        val channel = node["channel"].asText("your_channel_here").lowercase(Locale.getDefault())
        val botAuthToken = node["botAuthToken"].asText("acx963c12xi81i4qgsidchsdtzh1km")
        val commandDelayMillis = node["commandDelayMillis"].asLong(3500)
        val specialCommandDelayMillis = node["specialCommandDelayMillis"].asLong(60000)
        val randomSpawnCost = node["randomSpawnCost"].asInt(250)
        val commandPrefix = node["commandPrefix"].asText("#")

        fun getPointsSettings(): Points {
            return Points(node["points"])
        }

        fun getLinksSettings(): Links {
            return Links(node["links"])
        }

        class Points(node: JsonNode) {
            val perMinute = node["perMinute"].asInt(2)
            val subscriptions = node["subscriptions"].asInt(500)
            val bitMultiplier = node["bitMultiplier"].asInt(10)
            val whenDisabled = node["whenDisabled"].asBoolean(true)
        }

        class Links(node: JsonNode) {
            val help = node["help"].asText("")
            val spawn = node["spawn"].asText("")
            val give = node["give"].asText("")
            val weather = node["weather"].asText("")
            val misc = node["misc"].asText("")
        }
    }

    class TiltifySettings(node: JsonNode) {
        val enabled = node["enabled"].asBoolean(false)
        val campaignId = node["campaignId"].asText("your_campaign_id_here")
        val applicationToken = node["applicationToken"].asText("your_app_token_here")
        val pollingFrequency = node["pollingFrequencySecs"].asInt(2)
        val tagPrefix = node["tagPrefix"].asText("#")
        val defaultTag = node["defaultTag"].asText("default")
        val mockApi = node["mockApi"].asBoolean(false)
    }

    companion object {
        val settings = Settings()
    }
}
