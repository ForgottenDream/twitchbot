package uk.co.forgottendream.bethesdatwitchintegration.commands

import com.fasterxml.jackson.databind.ObjectMapper
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastCommandGlobal
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastDefaultCommand
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.allowedInInterior
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.npcAmountToSpawn
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.npcToSpawn
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.pluginToLookFor
import uk.co.forgottendream.bethesdatwitchintegration.json.PointLogic
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import uk.co.forgottendream.bethesdatwitchintegration.misc.MiscLogic
import java.io.FileReader
import java.util.*

object RandomSpawnCommand {
    private val mapper = ObjectMapper()

    fun handleRandomSpawn(username: String) {
        try {
            val filePath = "data/${settings.gameJson}/${settings.gameJson}spawns.json"
            val jsonNode = mapper.readTree(FileReader(filePath))
            val random = Random()
            val randomCommandIndex = random.nextInt(jsonNode.size())
            val commandName = jsonNode.fieldNames()
                .withIndex()
                .asSequence()
                .find { entry -> entry.index == randomCommandIndex }?.value ?: ""
            val randomArray = jsonNode[commandName]
            npcToSpawn = randomArray[1].asText()
            npcAmountToSpawn = randomArray[2].asInt()
            allowedInInterior = randomArray[3].asBoolean()
            pluginToLookFor = if (randomArray.size() == 5) {
                randomArray[4].asText()
            } else settings.defaultPlugin

            val allowedInInteriorValue: Int = if (allowedInInterior) 1 else 0

            val commandForXse = "spawn $npcToSpawn $npcAmountToSpawn $allowedInInteriorValue $pluginToLookFor"
            MiscLogic.createCommandCall(commandForXse)
            lastDefaultCommand = System.currentTimeMillis()
            lastCommandGlobal = System.currentTimeMillis()
            val randomSpawnCost = settings.getTwitchSettings().randomSpawnCost
            SpawnCommand.lastCommandFired = "$username $randomSpawnCost"
            PointLogic.modifyUserPoints(-randomSpawnCost, username)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }
}
