package uk.co.forgottendream.bethesdatwitchintegration.commands

import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.networking.TwitchCommand
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastCommandGlobal
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastDefaultCommand
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.allowedInInterior
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.npcAmountToSpawn
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.npcCost
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.npcToSpawn
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.pluginToLookFor
import uk.co.forgottendream.bethesdatwitchintegration.json.PointLogic
import uk.co.forgottendream.bethesdatwitchintegration.misc.MiscLogic
import java.util.*

object SpawnCommand {
    var lastCommandFired = "null 0"

    fun handleSpawn(username: String, spawnCommand: String) {
        val uuid = UUID.randomUUID().toString()
        BethesdaTwitchIntegration.messagingServer.twitchSpawns[uuid] = TwitchCommand(username, spawnCommand)
        BethesdaTwitchIntegration.messagingServer.sendIsInteriorMessage(uuid)
    }

    fun handleSpawn(username: String, spawnCommand: String?, isPlayerInside: Boolean) {
        if (CommandValidation.validateSpawn(spawnCommand)) {
            if (!allowedInInterior && isPlayerInside) {
                BethesdaTwitchIntegration.sendMessage("You can't spawn that inside, @$username.");
                return
            }

            val allowedInInteriorInt = if (allowedInInterior) 1 else 0

            if (PointLogic.userPoints(username) < npcCost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, npcCost)
            } else {
                PointLogic.modifyUserPoints(-npcCost, username)
                val commandForxSE = "spawn $npcToSpawn $npcAmountToSpawn $allowedInInteriorInt $pluginToLookFor"
                MiscLogic.createCommandCall(commandForxSE)
                lastDefaultCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
                lastCommandFired = "$username $npcCost"
            }
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
        }
    }
}
