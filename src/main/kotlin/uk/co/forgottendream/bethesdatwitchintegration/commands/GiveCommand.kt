package uk.co.forgottendream.bethesdatwitchintegration.commands

import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastCommandGlobal
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastDefaultCommand
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.itemCost
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.itemID
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.itemsMultiplied
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.pluginToLookFor
import uk.co.forgottendream.bethesdatwitchintegration.json.PointLogic
import uk.co.forgottendream.bethesdatwitchintegration.misc.MiscLogic

object GiveCommand {
    fun handleGive(username: String, giveCommand: String, amount: Int) {
        if (CommandValidation.validateGive(giveCommand)) {
            val cost = itemCost * amount

            if (PointLogic.userPoints(username) < cost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, cost)
            } else {
                val itemAmount: Int = if (itemsMultiplied) {
                    amount * 10
                } else {
                    amount
                }
                PointLogic.modifyUserPoints(-(cost), username)
                val commandForxSE = "player.additem $itemID $itemAmount $pluginToLookFor"
                MiscLogic.createCommandCall(commandForxSE)
                lastDefaultCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
            }
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
        }
    }
}
