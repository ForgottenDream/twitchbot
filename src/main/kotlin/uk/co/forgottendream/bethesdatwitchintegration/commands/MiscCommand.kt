package uk.co.forgottendream.bethesdatwitchintegration.commands

import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.miscCost
import uk.co.forgottendream.bethesdatwitchintegration.json.PointLogic
import uk.co.forgottendream.bethesdatwitchintegration.misc.MiscLogic
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastCommandGlobal
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastSpecialCommand

object MiscCommand {
    fun handleMisc(username: String, miscCommand: String) {
        if (CommandValidation.validateMisc(miscCommand)) {
            if (PointLogic.userPoints(username) < miscCost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, miscCost)
            } else {
                PointLogic.modifyUserPoints(-miscCost, username)
                MiscLogic.createCommandCall(CommandValidation.miscCommand)
                lastSpecialCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
            }
        }
    }
}
