package uk.co.forgottendream.bethesdatwitchintegration.commands

import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import java.util.*

object HelpCommand {
    private val linksSettings = settings.getTwitchSettings().getLinksSettings()

    fun handleHelpCommand(username: String, command: String) {
        val commandForHelp = command.lowercase(Locale.getDefault())
        var msg = "Help for #$commandForHelp, @$username: "

        when (commandForHelp) {
            "addpointsall" -> {
                msg += "[moderator only] Adds a given number of points to all channel users. e.g., #addpointsall 100"
            }
            "addpoints" -> {
                msg += "[moderator only] Adds a given number of points to a given channel user. e.g., #addpoints " +
                        "user1 100"
            }
            "points" -> {
                msg += "Returns the amount of points the channel user has."
            }
            "givepoints" -> {
                msg += "Lets a channel user give some of their own points to another channel user. e.g., #givepoints " +
                        "user1 100"
            }
            "purge" -> {
                msg += "[moderator only] Removes all data for channel users who fall below a given points threshold. " +
                        "e.g., #purge 500"
            }
            "toggle" -> {
                msg += "[moderator only] Enables/disables game modifying commands for the channel (#spawn, #give, " +
                        "#misc)"
            }
            "spawn" -> {
                msg += "Spend points to spawn NPCs in game, cost varies. e.g., #spawn house_cat. "

                val helpLink = linksSettings.spawn
                if (helpLink.isNotEmpty()) {
                    msg += "Go here for more info: $helpLink"
                }
            }
            "misc" -> {
                msg += "Spend points to cause a miscellaneous game effect, cost varies. e.g., #misc slow. "

                val helpLink = linksSettings.misc
                if (helpLink.isNotEmpty()) {
                    msg += "Go here for more info: $helpLink"
                }
            }
            "give" -> {
                msg += "Spend points to gives items to the player in game with an optional given amount, cost varies " +
                        "and is multiplied by the given amount. e.g., #give stimpak, #give ammo_5mm 5. "

                val helpLink = linksSettings.give
                if (helpLink.isNotEmpty()) {
                    msg += "Go here for more info: $helpLink"
                }
            }
            "randomspawn" -> {
                val cost = settings.getTwitchSettings().randomSpawnCost
                msg += "Spend points to spawn random NPCs in game, costs $cost."
            }
            "weather" -> {
                msg += "Spend points to change the weather in game, cost varies. e.g., #weather fog. "

                val helpLink = linksSettings.weather
                if (helpLink.isNotEmpty()) {
                    msg += "Go here for more info: $helpLink"
                }
            }
            "settingsrefresh" -> {
                msg += "[moderator only] Reloads the settings."
            }
            "testdonation" -> {
                msg += "[moderator only] Allows testing of Tiltify functions when using the mock server. e.g., " +
                        "#testdonation 20 user1 #commentTag"
            }
            else -> msg += "No help found, are you sure that command exists?"
        }

        BethesdaTwitchIntegration.sendMessage(msg)
    }
}
