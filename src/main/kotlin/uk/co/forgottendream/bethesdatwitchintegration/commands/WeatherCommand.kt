package uk.co.forgottendream.bethesdatwitchintegration.commands

import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.pluginToLookFor
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.weatherCost
import uk.co.forgottendream.bethesdatwitchintegration.json.CommandValidation.weatherID
import uk.co.forgottendream.bethesdatwitchintegration.json.PointLogic
import uk.co.forgottendream.bethesdatwitchintegration.misc.MiscLogic
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastCommandGlobal
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler.lastSpecialCommand

object WeatherCommand {
    fun handleWeather(username: String, weatherCommand: String) {
        if (CommandValidation.validateWeather(weatherCommand)) {
            if (PointLogic.userPoints(username) < weatherCost) {
                BethesdaTwitchIntegration.sendMissingPoints(username, weatherCost)
            } else {
                PointLogic.modifyUserPoints(-weatherCost, username)
                val commandForxSE = "weather $weatherID $pluginToLookFor"
                MiscLogic.createCommandCall(commandForxSE)
                lastSpecialCommand = System.currentTimeMillis()
                lastCommandGlobal = System.currentTimeMillis()
            }
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
        }
    }
}
