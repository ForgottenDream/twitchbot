package uk.co.forgottendream.bethesdatwitchintegration.commands

import com.github.twitch4j.TwitchClient
import com.github.twitch4j.common.enums.CommandPermission
import com.github.twitch4j.common.events.domain.EventUser
import uk.co.forgottendream.bethesdatwitchintegration.BethesdaTwitchIntegration
import uk.co.forgottendream.bethesdatwitchintegration.json.PointLogic
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import uk.co.forgottendream.bethesdatwitchintegration.json.TimedLogic
import uk.co.forgottendream.bethesdatwitchintegration.misc.MiscLogic
import uk.co.forgottendream.bethesdatwitchintegration.mockserver.TiltifyMockServer
import java.util.*

object CommandHandler {
    private val twitchSettings = settings.getTwitchSettings()
    var lastDefaultCommand = 0L
    var lastCommandGlobal = 0L
    var lastSpecialCommand = 0L

    fun handleCommand(
        eventUser: EventUser,
        command: Array<String>,
        twitchClient: TwitchClient,
        permissions: Set<CommandPermission>
    ) {
        val currentTimeMillis = System.currentTimeMillis()
        val commandToValidate = command[0].lowercase(Locale.getDefault()).substring(1)
        val username = eventUser.name

        //Checks with the impacted commands if the overhead system is flagged as enabled.
        if (!settings.systemEnabled) {
            when (commandToValidate) {
                "spawn", "misc", "give", "weather", "randomspawn" -> {
                    BethesdaTwitchIntegration.sendMessage("Twitch Integration is not currently enabled.")
                    return
                }
            }
        }

        /*
         * Low-Delay Command Time Verification
         */
        val lowDelayTime = lastDefaultCommand + twitchSettings.commandDelayMillis
        if (currentTimeMillis <= lowDelayTime || currentTimeMillis <= lastCommandGlobal + 250) {
            when (commandToValidate) {
                "spawn", "give", "randomspawn" -> {
                    BethesdaTwitchIntegration.sendMessage("Too many commands, please wait a bit.")
                    return
                }
            }
        }

        /*
         * High-Delay Command Time Verification
         */
        val highDelayTime = lastSpecialCommand + twitchSettings.specialCommandDelayMillis
        if (currentTimeMillis <= highDelayTime || currentTimeMillis <= lastCommandGlobal + 250) {
            when (commandToValidate) {
                "weather", "misc" -> {
                    BethesdaTwitchIntegration.sendMessage("Too many commands, please wait a bit.")
                    return
                }
            }
        }

        when (commandToValidate) {
            "addpointsall" -> if (command.size == 2) {
                if (higherPermissions(username, permissions)) {
                    addPointsAll(command, username)
                } else {
                    BethesdaTwitchIntegration.sendPermissionsProblem(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "addpoints" -> if (command.size == 3) {
                if (higherPermissions(username, permissions)) {
                    val userParam = command[1].lowercase(Locale.getDefault())
                    val pointsParam = command[2]

                    if (PointLogic.userExists(userParam) && MiscLogic.isInteger(pointsParam)) {
                        if (username.equals(userParam, ignoreCase = true)) {
                            BethesdaTwitchIntegration.sendMessage("You can't add points to yourself @$username")
                        } else {
                            val points = pointsParam.toInt()
                            PointLogic.modifyUserPoints(points, userParam)
                            BethesdaTwitchIntegration.sendAddPointsMessage(username, points, userParam)
                        }
                    } else {
                        BethesdaTwitchIntegration.sendInvalidCommand(username)
                    }
                } else {
                    BethesdaTwitchIntegration.sendPermissionsProblem(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "points" -> if (command.size == 1) {
                if (isJoined(username)) {
                    val tempValue: Int = PointLogic.userPoints(username)
                    if (tempValue >= 0) {
                        BethesdaTwitchIntegration.sendMessage("You have $tempValue points @$username")
                    } else {
                        BethesdaTwitchIntegration.sendMessage("You have 0 points @$username")
                    }
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "givepoints" -> if (command.size == 3) {
                val userParam = command[1].lowercase(Locale.getDefault())
                val pointsParam = command[2]

                if (isJoined(username) && MiscLogic.isInteger(pointsParam) && !userParam.equals(username, ignoreCase = true)) {
                    val points = pointsParam.toInt()

                    if (PointLogic.userPoints(username) >= points) {
                        if (PointLogic.userExists(userParam)) {
                            PointLogic.modifyUserPoints(-points, username)
                            PointLogic.modifyUserPoints(points, userParam)
                            val msg = "@$username gave $points points to @$userParam"
                            BethesdaTwitchIntegration.sendMessage(msg)
                        } else {
                            BethesdaTwitchIntegration.sendMessage("The user you are trying to give points to has not joined BTTI, @$username")
                        }
                    } else {
                        BethesdaTwitchIntegration.sendMissingPoints(username, points)
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "purge" -> if (command.size == 2) {
                val pointsParam = command[1]

                if (higherPermissions(username, permissions)) {
                    if (MiscLogic.isInteger(pointsParam)) {
                        PointLogic.purgeUsers(pointsParam.toInt())
                    } else {
                        BethesdaTwitchIntegration.sendInvalidCommand(username)
                    }
                } else {
                    BethesdaTwitchIntegration.sendPermissionsProblem(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "toggle" -> if (command.size == 1) {
                if (permissions.contains(CommandPermission.BROADCASTER)) {
                    settings.systemEnabled = !settings.systemEnabled
                    if (settings.systemEnabled) {
                        PointLogic.initActiveState(eventUser.id, twitchClient)
                        BethesdaTwitchIntegration.sendMessage("Enabled Twitch Integration.")
                    } else {
                        BethesdaTwitchIntegration.sendMessage("Disabled Twitch Integration.")
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "tada" -> if (username.equals("Flenarn_", ignoreCase = true)) {
                val msg = "Hey, look everyone! It's @flenarn_, he developed BTTI's progenitor, BTI!"
                BethesdaTwitchIntegration.sendMessage(msg)
            } else if (username.equals("VoodooFrog", ignoreCase = true)) {
                val msg = "Hey, look everyone! It's @VoodooFrog, he develops BTTI!"
                BethesdaTwitchIntegration.sendMessage(msg)
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "spawn" -> if (command.size == 2) {
                if (isJoined(username)) {
                    val spawnIdParam = command[1]
                    SpawnCommand.handleSpawn(username, spawnIdParam)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "misc" -> {
                if (command.size == 2) {
                    if (isJoined(username)) {
                        val miscIdParam = command[1]
                        MiscCommand.handleMisc(username, miscIdParam)
                    }
                } else {
                    BethesdaTwitchIntegration.sendInvalidCommand(username)
                }
            }

            "give" -> if (command.size == 3) {
                if (isJoined(username)) {
                    val itemIdParam = command[1]
                    val amountParam = command[2]
                    val amount: Int = if (MiscLogic.isInteger(amountParam)) {
                        amountParam.toInt()
                    } else {
                        BethesdaTwitchIntegration.sendInvalidCommand(username)
                        return
                    }

                    if (amount > 1000) {
                        BethesdaTwitchIntegration.sendMessage("Max amount to spawn is 1000 @$username")
                    } else {
                        GiveCommand.handleGive(username, itemIdParam, amount)
                    }
                }
            } else if (command.size == 2) {
                if (isJoined(username)) {
                    val itemIdParam = command[1]
                    GiveCommand.handleGive(username, itemIdParam, 1)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "randomspawn" -> if (command.size == 1) {
                if (isJoined(username)) {
                    if (PointLogic.userPoints(username) < twitchSettings.randomSpawnCost) {
                        BethesdaTwitchIntegration.sendMissingPoints(username, twitchSettings.randomSpawnCost)
                    } else {
                        RandomSpawnCommand.handleRandomSpawn(username)
                    }
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "weather" -> if (command.size == 2) {
                if (isJoined(username)) {
                    val weatherIdParam = command[1]
                    WeatherCommand.handleWeather(username, weatherIdParam)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "settingsrefresh" -> if (command.size == 1) {
                if (higherPermissions(username, permissions)) {
                    if (settings.refreshSettings()) {
                        BethesdaTwitchIntegration.sendMessage("Settings refreshed.")
                    } else {
                        BethesdaTwitchIntegration.sendMessage("An error occurred refreshing settings, check logs.")
                    }
                } else {
                    BethesdaTwitchIntegration.sendPermissionsProblem(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "help" -> if (command.size == 1) {
                var msg = "Help for @$username: Here's the list of commands - #reload, #addpointsall, #addpoints, " +
                        "#points, #givepoints, #purge, #toggle, #tada, #spawn, #misc, #give, #randomspawn, #weather, " +
                        "#settingsrefresh, #testdonation. To get help on a command type #help <command> where " +
                        "command is the command name without the '#'. "

                val helpLink = twitchSettings.getLinksSettings().help

                if (helpLink.isNotEmpty()) {
                    msg += "Go here for more info: $helpLink"
                }

                BethesdaTwitchIntegration.sendMessage(msg)
            } else {
                HelpCommand.handleHelpCommand(username, command[1])
            }

            "testdonation" -> if (command.size >= 4) {
                if (higherPermissions(username, permissions)) {
                    val donationAmount = command[1].toFloat()
                    val name = command[2]
                    var comment = ""

                    for (i in 3 until command.size) {
                        comment += "${command[i]} "
                    }

                    if (settings.getTiltifySettings().mockApi) {
                        TiltifyMockServer.createData(donationAmount, name, comment.trim())
                    } else {
                        BethesdaTwitchIntegration.sendMessage("Tiltify mock server is not running.")
                    }
                } else {
                    BethesdaTwitchIntegration.sendPermissionsProblem(username)
                }
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            "joinbtti" -> if (command.size == 1) {
                TimedLogic.addUser(username)
                BethesdaTwitchIntegration.sendMessage("You have joined BTTI, @$username")
            } else {
                BethesdaTwitchIntegration.sendInvalidCommand(username)
            }

            else -> BethesdaTwitchIntegration.sendMessage("Invalid command @$username")
        }
    }

    private fun addPointsAll(command: Array<String>, username: String) {
        if (MiscLogic.isInteger(command[1])) {
            val chatters = PointLogic.getActiveUsers()
            val points = command[1].toInt()
            for (userName in chatters) {
                PointLogic.modifyUserPoints(points, username)
            }
            BethesdaTwitchIntegration.sendAddPointsMessage(username, points)
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
        }
    }

    /*
     * Basic permission check to see if command-caller is moderator or the streamer.
     */
    private fun higherPermissions(username: String, permissions: Set<CommandPermission>): Boolean {
        return if (permissions.contains(CommandPermission.MODERATOR) || permissions.contains(CommandPermission.BROADCASTER)
        ) {
            true
        } else {
            BethesdaTwitchIntegration.sendInvalidCommand(username)
            false
        }
    }

    private fun isJoined(username: String): Boolean {
        return if (PointLogic.userExists(username)) {
            true
        } else {
            val commandPrefix = settings.getTwitchSettings().commandPrefix
            BethesdaTwitchIntegration.sendMessage("You haven't joined with ${commandPrefix}joinbtti, @$username")
            false
        }
    }
}
