package uk.co.forgottendream.bethesdatwitchintegration

import com.github.philippheuer.credentialmanager.domain.OAuth2Credential
import com.github.twitch4j.TwitchClientBuilder
import com.github.twitch4j.chat.events.channel.*
import mu.KotlinLogging
import uk.co.forgottendream.bethesdatwitchintegration.commands.CommandHandler
import uk.co.forgottendream.bethesdatwitchintegration.json.PointLogic
import uk.co.forgottendream.bethesdatwitchintegration.json.Settings.Companion.settings
import uk.co.forgottendream.bethesdatwitchintegration.json.TimedLogic
import uk.co.forgottendream.bethesdatwitchintegration.mockserver.TiltifyMockServer
import uk.co.forgottendream.bethesdatwitchintegration.networking.MessagingServer
import uk.co.forgottendream.bethesdatwitchintegration.tiltify.TiltifyVariant
import java.io.InputStream
import java.util.*

class BethesdaTwitchIntegration {
    init {
        val twitchSettings = settings.getTwitchSettings()
        val pointsSettings = twitchSettings.getPointsSettings()
        messagingServer.start()

        val props = javaClass.classLoader.getResourceAsStream("version.properties").use {
            Properties().apply { load(it) }
        }

        logInfo(message = "Running version ${props["version"]}")
        logInfo("Twitch", "Connected to channel ${twitchSettings.channel}.")

        twitchClient.chat.joinChannel(twitchSettings.channel)

        twitchClient.eventManager.onEvent(CheerEvent::class.java) { e: CheerEvent ->
            if (!PointLogic.userExists(e.user.name.lowercase(Locale.getDefault()))) {
                PointLogic.addUser(e.user.name.lowercase(Locale.getDefault()))
            }
            PointLogic.modifyUserPoints(
                (e.bits * pointsSettings.bitMultiplier),
                e.user.name.lowercase(Locale.getDefault())
            )
        }

        /*
         * Handles the subscription event.
         *
         * Points get multiplied by tier level (1, 2, 3).
         *
         * Only fires if it's not a gifted subscription.
         */
        twitchClient.eventManager.onEvent(SubscriptionEvent::class.java) { e: SubscriptionEvent ->
            if (!e.gifted) {
                var levelMultiplier = 0
                when (e.subscriptionPlan) {
                    "1000" -> levelMultiplier = 1
                    "2000" -> levelMultiplier = 2
                    "3000" -> levelMultiplier = 3
                }
                if (!PointLogic.userExists(e.user.name.lowercase(Locale.getDefault()))) {
                    PointLogic.addUser(e.user.name.lowercase(Locale.getDefault()))
                }
                PointLogic.modifyUserPoints(
                    pointsSettings.subscriptions * levelMultiplier,
                    e.user.name.lowercase(Locale.getDefault())
                )
            }
        }

        /*
        * Handles the gift sub event.
        *
        * Points get multiplied by tier level (1, 2, 3) and then by sub amount.
        */
        twitchClient.eventManager.onEvent(GiftSubscriptionsEvent::class.java) { e: GiftSubscriptionsEvent ->
            var levelMultiplier = 0
            when (e.subscriptionPlan) {
                "1000" -> levelMultiplier = 1
                "2000" -> levelMultiplier = 2
                "3000" -> levelMultiplier = 3
            }
            if (!PointLogic.userExists(e.user.name.lowercase(Locale.getDefault()))) {
                PointLogic.addUser(e.user.name.lowercase(Locale.getDefault()))
            }
            PointLogic.modifyUserPoints(
                pointsSettings.subscriptions * levelMultiplier, e.user.name.lowercase(Locale.getDefault())
            )
        }

        twitchClient.eventManager.onEvent(ChannelMessageEvent::class.java) { e: ChannelMessageEvent ->
            if (e.message.substring(0, 1).equals(twitchSettings.commandPrefix, ignoreCase = true)) {
                val commandString = e.message.split(" ").toTypedArray()
                CommandHandler.handleCommand(
                    e.user,
                    commandString,
                    twitchClient,
                    e.permissions
                )
            }
        }

        twitchClient.eventManager.onEvent(ChannelLeaveEvent::class.java) { e: ChannelLeaveEvent ->
            PointLogic.setActiveState(e.user.name, false)
        }

        twitchClient.eventManager.onEvent(ChannelJoinEvent::class.java) {e: ChannelJoinEvent ->
            PointLogic.setActiveState(e.user.name, true)
        }

        twitchPointsLoop()

        if (settings.getTiltifySettings().enabled) {
            if (settings.getTiltifySettings().mockApi) {
                logInfo("Tiltify", "Starting MockServer")
                TiltifyMockServer()
            }
            logInfo("Tiltify", "Connected to campaign ${twitchSettings.channel}")
            TiltifyVariant.tiltifyLoop()
        }
    }

    companion object {
        private val twitchClient = TwitchClientBuilder.builder()
            .withEnableHelix(true)
            .withChatAccount(
                OAuth2Credential("twitch", "oauth:${settings.getTwitchSettings().botAuthToken}")
            )
            .withEnableChat(true)
            .build()
        private val logger = KotlinLogging.logger {}
        val messagingServer = MessagingServer(settings.getPort())
//        var channelId: String? = null
        val userInfo = twitchClient.helix
            .getUsers(settings.getTwitchSettings().botAuthToken, null, null).execute()
        val botUserId = userInfo.users[0].id

        fun sendMessage(message: String?) {
            twitchClient.chat.sendMessage(settings.getTwitchSettings().channel, message)
        }

        fun sendAddPointsMessage(username: String, points: Int, receivingUser: String = "everyone") {
            val msg = "@$username just added $points points to $receivingUser!"
            twitchClient.chat.sendMessage(settings.getTwitchSettings().channel, msg)
        }

        fun sendInvalidCommand(username: String) {
            twitchClient.chat.sendMessage(settings.getTwitchSettings().channel, "Invalid command @$username")
        }

        fun sendPermissionsProblem(username: String) {
            val msg = "You don't have the permissions to do that @$username"
            twitchClient.chat.sendMessage(settings.getTwitchSettings().channel, msg)
        }

        fun sendMissingPoints(username: String, cost: Int) {
            twitchClient.chat.sendMessage(
                settings.getTwitchSettings().channel,
                "You need $cost to do that, you have ${PointLogic.userPoints(username)} @$username"
            )
        }

        fun logInfo(tag: String = "", message: String) {
            if (tag.isNotEmpty()) {
                println("[$tag] $message")
                logger.info("[$tag] $message")
            } else {
                println(message)
                logger.info(message)
            }
        }

        fun logCatch(tag: String = "", throwable: Throwable) {
            if (tag.isNotEmpty()) {
                println("[$tag] ${throwable.message}")
            } else {
                println(throwable.message)
            }

            logger.catching(throwable)
        }

        fun logError(tag: String = "", message: String) {
            if (tag.isNotEmpty()) {
                println("[$tag] $message")
                logger.error("[$tag] $message")
            } else {
                println(message)
                logger.error(message)
            }
        }

        fun getFileFromResourceAsStream(fileName: String): InputStream {
            val classLoader = Companion::class.java.classLoader
            val inputStream = classLoader.getResourceAsStream(fileName)
            return inputStream ?: throw IllegalArgumentException("file not found! $fileName")
        }

        fun twitchPointsLoop() {
            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    if (settings.systemEnabled || settings.getTwitchSettings().getPointsSettings().whenDisabled) {
                        TimedLogic.defaultPointLoop()
                    }
                }
            }, 0, (60 * 1000).toLong())
        }
    }
}
